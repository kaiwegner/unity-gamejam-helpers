using System;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

namespace GameJam
{
	public static class Helpers
	{

		#region assets

		/// <summary>
		/// Loads an asset from the Resources folder. File extensions should be omitted. 
		/// Example:
		/// You want to load : Assets/Resources/myFolder/myTest.png
		/// Your code: LoadResource<Texture2D>("myFolder/myTest");
		/// </summary>
		/// <returns>The resource.</returns>
		/// <param name="pPath">P path.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T LoadResource<T>(string pPath) where T : UnityEngine.Object
		{
			return Resources.Load( pPath) as T;
		}

		/// <summary>
		/// Returns one of the objects in the given array
		/// </summary>
		/// <returns>The random.</returns>
		/// <param name="pObjects">P objects.</param>
		public static T GetRandom<T>(T[] pObjects)
		{
			if(pObjects.Length == 1)
				return pObjects[0];

			if(pObjects.Length < 1)
				throw new IndexOutOfRangeException("No objects in array.");

			System.Random rnd = new System.Random();
			return pObjects[ rnd.Next(0, pObjects.Length) ];
		}

		#endregion

		#region input


		public static Vector3 RandomInBounds(Bounds b)
		{
			Vector3 o = b.center;
			o += Vector3.Scale(b.size, new Vector3(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value)) - b.size * 0.5f;
			return o;
		}

		/// <summary>
		/// Using the mainCamera tries to cast a ray through the given mouse position and 
		/// checks if it hits the given collider. 
		/// </summary>
		/// <returns><c>true</c>, if test was hit, <c>false</c> otherwise.</returns>
		/// <param name="pCollider">P collider.</param>
		/// <param name="pMousePosition">P mouse position.</param>
		public static bool HitTest(Collider pCollider, Vector3 pMousePosition)
		{
			RaycastHit hit;
			Ray r = Camera.main.ScreenPointToRay(pMousePosition);

			if(pCollider.Raycast(r, out hit, 10000))
				return true;

			return false;
		}

		/// <summary>
		/// Using the mainCamera tries to cast a ray through the given mouse position and 
		/// checks if it hits the given collider. 
		/// </summary>
		/// <returns><c>true</c>, if test was hit, <c>false</c> otherwise.</returns>
		/// <param name="pCollider">P collider.</param>
		/// <param name="pMousePosition">P mouse position.</param>
		public static bool HitTest(Collider2D pCollider, Vector3 pMousePosition)
		{
			Vector2 p = Camera.main.ScreenToWorldPoint(pMousePosition);
			if(pCollider.OverlapPoint(p))
				return true;
			return false;
		}

		#endregion

		#region serialization
		/// <summary>
		/// Using an XML serializer, saved the given object with the given filename. (No file extension needed)
		/// </summary>
		public static void Save(object pObject, string pPath)
		{
			XmlSerializer serializer = new XmlSerializer(pObject.GetType());
			
			using (StringWriter writer = new StringWriter())
			{
				serializer.Serialize(writer, pObject);

				File.WriteAllText( Application.persistentDataPath + pPath + ".xml", writer.ToString());

			}


		}

		/// <summary>
		/// Using an XML serializer, loads the object stored with the given filename. (No file extension needed)
		/// </summary>
		public static T Load<T>(string pPath) where T : class
		{		
			string path = Application.persistentDataPath + pPath + ".xml";
			XmlSerializer deserializer = new XmlSerializer(typeof(T));
			try
			{
				string content = File.ReadAllText(path);
				using(StringReader reader = new StringReader(content))
				{
					return deserializer.Deserialize (reader) as T;
				}
			}catch
			{
				Debug.LogError("Could not load file from " + pPath + " (" +  path +")");

			}
			return null;
		}
		#endregion

		#region ui
		private static Texture2D mWhiteTexture;
		/// <summary>
		/// Returns a generated 4x4 texture with white pixels
		/// </summary>
		/// <value>The white texture.</value>
		public static Texture2D whiteTexture
		{
			get {
				if(mWhiteTexture == null)
				{
					mWhiteTexture = new Texture2D(4,4);
					for(int x = 0;x < 4;x++)
						for(int y = 0;y < 4;y++)
							mWhiteTexture.SetPixel(x,y,Color.white);
					mWhiteTexture.Apply();
					mWhiteTexture.Compress(true);
				}
				return mWhiteTexture;
			}
		}

		
		private static Texture2D mBlackTexture;
		/// <summary>
		/// Returns a generated 4x4 texture with black pixels. 
		/// </summary>
		/// <value>The black texture.</value>
		public static Texture2D blackTexture
		{
			get {
				if(mBlackTexture == null)
				{
					mBlackTexture = new Texture2D(4,4);
					for(int x = 0;x < 4;x++)
						for(int y = 0;y < 4;y++)
							mBlackTexture.SetPixel(x,y,Color.black);
					mBlackTexture.Apply();
					mBlackTexture.Compress(true);
				}
				return mBlackTexture;
			}
		}
		#endregion
	}
}