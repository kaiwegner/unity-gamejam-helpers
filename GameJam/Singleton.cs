using System;
using UnityEngine;

namespace GameJam
{
	public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		public static T instance
		{
			get;
			private set;
		}

		void OnEnable()
		{
			instance = this as T;
		}

	}
}

